@echo off

REM usage:
REM ./decrypt.sh foo.zip dest.path openssl_path

set TEMP_DIR=enc
set KEY_FILE=secret.txt.key

set TAR_FILE=%1
set MAIN_PATH=%2

if [%3]==[] (
	set SSLP=.\tools\openssl-1.0.2j-x64_86-win64\openssl.exe
) else (
	set SSLP=%3
)

md %TEMP_DIR%

set GZIPP=.\tools\gzip.exe
set TARP=.\tools\tar.exe

%GZIPP% -d < %TAR_FILE% | %TARP% -xvf - -C %TEMP_DIR%

for /r %TEMP_DIR% %%f in (*) do (
	@move "%%f" %TEMP_DIR%
)

for /r %TEMP_DIR% %%a in (*.encfile) do (
	set MAIN_ENC=%%~nxa
	set MAIN_FILE=%%~na
)
for /r %TEMP_DIR% %%a in (*.enckey) do (
	set KEY_ENC=%%~nxa
	set KEY_FILE=%%~na
)

%SSLP% rsautl -decrypt -ssl -inkey .ssh/id_rsa -in %TEMP_DIR%/%KEY_ENC% -out %TEMP_DIR%/%KEY_FILE%
%SSLP% aes-256-cbc -d -in %TEMP_DIR%/%MAIN_ENC% -out %MAIN_FILE% -pass file:%TEMP_DIR%/%KEY_FILE%
@RD /S /Q %TEMP_DIR%
