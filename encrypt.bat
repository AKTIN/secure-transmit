@echo off

REM usage:
REM ./encrypt.sh secret.txt foo.tgz openssl_path

set TEMP_DIR=enc
set KEY_FILE=secret.txt.key

set MAIN_FULL=%1
set MAIN_PATH=%~p1
set MAIN_FILE=%~nx1
set TAR_FILE=%2

if [%3]==[] (
	set SSLP=.\openssl-1.0.2j-x64_86-win64\openssl.exe
) else (
	set SSLP=%3
)



REM make random key
mkdir %TEMP_DIR%
%SSLP% rand 192 -out %TEMP_DIR%/%KEY_FILE%

%SSLP% aes-256-cbc -in %MAIN_FULL% -out %TEMP_DIR%/%MAIN_FILE%.encfile -pass file:%TEMP_DIR%/%KEY_FILE%
%SSLP% rsautl -encrypt -pubin -inkey .ssh\id_rsa.pub.pkcs8 -in %TEMP_DIR%/%KEY_FILE% -out %TEMP_DIR%/%KEY_FILE%.enckey

zip -j %TAR_FILE% %TEMP_DIR%/*.encfile %TEMP_DIR%/*.enckey
@RD /S /Q %TEMP_DIR%
